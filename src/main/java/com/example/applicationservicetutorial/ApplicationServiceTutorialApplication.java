package com.example.applicationservicetutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationServiceTutorialApplication {

  public static void main(String[] args) {
    SpringApplication.run(ApplicationServiceTutorialApplication.class, args);
  }

}
