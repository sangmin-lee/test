package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserId;
import com.example.applicationservicetutorial.user.domain.UserService;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class UserUpdateService {

  private final UserRepository userRepository;
  private final UserService userService;

  public UserUpdateService(final UserRepository userRepository, final UserService userService) {
    this.userRepository = userRepository;
    this.userService = userService;
  }

  @Transactional
  public void handle(final UserUpdateCommand command) {
    User user = findUserById(command.getId());

    String name = command.getUserFullName();
    if (name != null) {
      UserFullName newUserFullName = new UserFullName(name);
      this.userRepository.delete(user);
      user.changeName(newUserFullName);
      checkExists(user);
      this.userRepository.save(user);
    }
  }

  private void checkExists(final User user) {
    if (this.userService.exists(user)) {
      throw new CanNotRegisterUserException(user, "이미 등록된 사용자입니다.");
    }
  }

  private User findUserById(final String id) {
    final UUID uuid = UUID.fromString(id);
    final UserId idToFind = new UserId(uuid);
    return this.userRepository.findById(idToFind).orElseThrow(() -> {
      throw new UserNotFoundException(uuid, "존재하지 않는 사용자입니다.");
    });
  }

}
