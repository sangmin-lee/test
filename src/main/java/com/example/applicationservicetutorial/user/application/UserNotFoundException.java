package com.example.applicationservicetutorial.user.application;

import java.util.UUID;

public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(UUID uuid, String message) {
    super(uuid + " : " + message);
  }

}
