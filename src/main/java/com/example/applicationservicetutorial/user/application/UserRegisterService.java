package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserService;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserRegisterService {

  private final UserRepository userRepository;
  private final UserService userService;

  @Autowired
  public UserRegisterService(final UserRepository userRepository, final UserService userService) {
    this.userRepository = userRepository;
    this.userService = userService;
  }

  @Transactional
  public void handle(final UserRegisterCommand command) {
    final UserFullName userFullName = new UserFullName(command.getUserFullName());
    final User user = new User(userFullName);

    if (this.userService.exists(user)) {
      throw new CanNotRegisterUserException(user, "이미 등록된 사용자입니다");
    }

    this.userRepository.save(user);
  }

}
