package com.example.applicationservicetutorial.user.application;

public class UserRegisterCommand {

  private String userFullName;

  public UserRegisterCommand(String userFullName) {
    this.userFullName = userFullName;
  }

  public String getUserFullName() {
    return userFullName;
  }

}
