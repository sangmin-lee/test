package com.example.applicationservicetutorial.user.application;

public class UserUpdateCommand {

  private String id;
  private String name;

  public UserUpdateCommand(final String id, final String name) {
    this.id = id;
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public String getUserFullName() {
    return name;
  }

}
