package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.UserId;
import com.example.applicationservicetutorial.user.domain.UserService;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class UserApplicationService {

  private final UserRepository userRepository;
  private final UserService userService;

  public UserApplicationService(final UserRepository userRepository,
                                final UserService userService) {
    this.userRepository = userRepository;
    this.userService = userService;
  }

  @Transactional
  public void register(final String name) {
    User user = new User(new UserFullName(name));

    checkExits(user);
    userRepository.save(user);
  }

  private void checkExits(final User user) {
    if (userService.exists(user)) {
      throw new CanNotRegisterUserException(user, "이미 등록된 사용자입니다.");
    }
  }

  // 도메인 객체 반환
//  public User get(final String uuidStr) {
//    UUID uuid = UUID.fromString(uuidStr);
//    UserId userId = new UserId(uuid);
//    Optional<User> user = userRepository.findById(userId);
//    return user.orElseThrow(() -> {
//      throw new NotFoundUserException(uuid, "존재하지 않는 사용자입니다.");
//    });
//  }

  // DTO 객체 반환
//  public UserDto get(final String uuidStr) {
//    UUID uuid = UUID.fromString(uuidStr);
//    UserId userId = new UserId(uuid);
//    User foundUser = userRepository.findById(userId).orElseThrow(() -> {
//      throw new NotFoundUserException(uuid, "존재하지 않는 사용자입니다.");
//    });
//    return new UserDto(
//        foundUser.getUserId().getValue().toString(),
//        foundUser.getUserFullName().getValue());
//  }

  // DTO 객체 반환 & DTO 생성자 파라미터 수정
  public UserDto get(final String uuid) {
    return new UserDto(findUserById(uuid));
  }

  @Transactional
  public void update(final UserUpdateCommand command) {
    User foundUser = findUserById(command.getId());

    String name = command.getUserFullName();
    if (name != null) {
      UserFullName newUserFullName = new UserFullName(name);
      userRepository.delete(foundUser);
      foundUser.changeName(newUserFullName);
      checkExits(foundUser);
      userRepository.save(foundUser);
    }
  }

  private User findUserById(final String id) {
    UUID uuid = UUID.fromString(id);
    UserId idToFind = new UserId(uuid);
    return userRepository.findById(idToFind).orElseThrow(() -> {
      throw new UserNotFoundException(uuid, "존재하지 않는 사용자입니다.");
    });
  }

  @Transactional
  public void delete(final UserDeleteCommand command) {
    User foundUser = findUserById(command.getId());
    userRepository.delete(foundUser);
  }

}
