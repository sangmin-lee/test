package com.example.applicationservicetutorial.user.application;

public class UserDeleteCommand {

  private String id;

  public UserDeleteCommand(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

}
