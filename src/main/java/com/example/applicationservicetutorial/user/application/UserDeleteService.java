package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserId;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class UserDeleteService {

  private final UserRepository userRepository;

  @Autowired
  public UserDeleteService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Transactional
  public void handle(final UserDeleteCommand command) {
    final UUID uuid = UUID.fromString(command.getId());
    final UserId userId = new UserId(uuid);
    final User user = this.userRepository.findById(userId)
        .orElseThrow(() -> {
          throw new UserNotFoundException(uuid, "존재하지 않는 사용자입니다.");
        });
    this.userRepository.delete(user);
  }

}
