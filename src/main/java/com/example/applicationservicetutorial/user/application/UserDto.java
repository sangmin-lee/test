package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.User;

public class UserDto {

  private String id;
  private String name;

  public UserDto(final User user) {
    id = user.getUserId().getUserId().toString();
    name = user.getUserFullName().getName();
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

}
