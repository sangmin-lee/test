package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.User;

public class CanNotRegisterUserException extends RuntimeException {

  public CanNotRegisterUserException(final User user, final String message) {
    super(user.getUserFullName().getName() + " : " + message);
  }

}
