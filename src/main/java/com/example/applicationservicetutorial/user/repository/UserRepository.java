package com.example.applicationservicetutorial.user.repository;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, UserId> {

//  Optional<User> findByUserFullName(final UserFullName userFullName);

  boolean existsByUserFullName(final UserFullName userFullName);

}
