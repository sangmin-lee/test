package com.example.applicationservicetutorial.user.domain;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.util.Objects;
import java.util.UUID;

@Entity
public class User {

  @EmbeddedId
  @GeneratedValue
  private UserId id;

  @Embedded
  private UserFullName userFullName;

  protected User() {
  }

  // 인스턴스를 처음 생성할 때 사용한다.
  public User(final UserFullName userFullName) {
    if (userFullName == null) {
      throw new IllegalArgumentException("이름이 존재해야 합니다.");
    }
    this.id = new UserId(UUID.randomUUID());
    this.userFullName = userFullName;
  }

  // 인스턴스를 복원할 때 사용한다.
  public User(final UserId id, final UserFullName userFullName) {
    if (id == null) {
      throw new IllegalArgumentException("ID가 존재해야 합니다.");
    }

    if (userFullName == null) {
      throw new IllegalArgumentException("이름이 존재해야 합니다.");
    }

    this.id = id;
    this.userFullName = userFullName;
  }

  public void changeName(final UserFullName userFullName) {
    if (userFullName == null) {
      throw new IllegalArgumentException("이름이 존재해야 합니다.");
    }

    this.userFullName = userFullName;
  }

  public UserId getUserId() {
    return id;
  }

  public UserFullName getUserFullName() {
    return userFullName;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return Objects.equals(id, user.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

}
