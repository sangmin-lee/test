package com.example.applicationservicetutorial.user.domain;

import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private final UserRepository userRepository;

  @Autowired
  public UserService(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public boolean exists(final User user) {
    return userRepository.existsByUserFullName(user.getUserFullName());
  }

}
