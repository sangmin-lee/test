package com.example.applicationservicetutorial.user.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class UserFullName {

  private String name;

  private UserFullName() {
  }

  public UserFullName(final String name) {
    if (name == null || name.isBlank()) {
      throw new IllegalArgumentException("이름이 존재해야 합니다.");
    }

    if (name.length() < 3) {
      throw new IllegalArgumentException("이름이 3글자 이상이어야 합니다.");
    }

    if (name.length() > 20) {
      throw new IllegalArgumentException("이름이 20글자 이하이어야 합니다");
    }

    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserFullName that = (UserFullName) o;
    return Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}
