package com.example.applicationservicetutorial.user.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class UserId implements Serializable {

  private UUID userId;

  private UserId() {
  }

  public UserId(final UUID userId) {
    if (userId == null) {
      throw new IllegalArgumentException("ID가 존재해야 합니다.");
    }
    this.userId = userId;
  }

  public UUID getUserId() {
    return userId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserId userId = (UserId) o;
    return Objects.equals(this.userId, userId.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId);
  }

}
