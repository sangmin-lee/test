package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@Transactional
class UserRegisterServiceTest {

  @Autowired
  UserRegisterService service;
  @Autowired
  UserRepository repository;

  @Test
  @DisplayName("회원가입 성공")
  void givenUserRegisterCommandWhenCallHandleThenVoid() {
    // given
    UserRegisterCommand command = new UserRegisterCommand("abc");

    // when
    service.handle(command);

    // then
    String name = command.getUserFullName();
    UserFullName newUserFullName = new UserFullName(name);
    boolean exists = repository.existsByUserFullName(newUserFullName);
    assertThat(exists).isTrue();
  }

}