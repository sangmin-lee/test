package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserId;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Transactional
class UserApplicationServiceTest {

  @Autowired
  UserApplicationService applicationService;
  @Autowired
  UserRepository repository;

  @Test
  @DisplayName("회원가입 성공")
  void givenNameStringWhenCallRegisterThenVoid() {
    // given
    String name = "abc";
    UserFullName userFullName = new UserFullName(name);

    // when
    applicationService.register(name);

    // then
    boolean present = repository.existsByUserFullName(userFullName);
    assertThat(present).isTrue();
  }

  @Test
  @DisplayName("회원가입시 중복된 Name일때 예외 발생")
  void givenDuplicateNameWhenCallRegisterThenThrowException() {
    // given
    String duplicateName = "abc";
    User user = new User(new UserFullName(duplicateName));
    repository.save(user);

    // when
    // then
    String message = assertThrows(CanNotRegisterUserException.class, () -> applicationService.register(duplicateName))
        .getMessage();
    assertThat(message).contains("이미 등록된 사용자입니다.");
  }

  // 도메인 객체 반환
//  @Test
//  @DisplayName("UUID로 사용자 조회 성공")
//  void givenUUIDWhenCallGetThenReturnUser() {
//    // given
//    User user = new User(new Name("abc"));
//    repository.save(user);
//
//    // when
//    User foundUser = service.get(user.getUserId().getValue().toString());
//
//    // then
//    assertThat(foundUser).isNotNull();
//    assertThat(foundUser.getUserFullName()).isEqualTo(user.getUserFullName());
//  }

  // DTO 객체 반환
  @Test
  @DisplayName("UUID로 사용자 조회 성공")
  void givenUUIDWhenCallGetThenReturnUser() {
    // given
    User user = new User(new UserFullName("abc"));
    repository.save(user);

    // when
    UserDto foundUser = applicationService.get(user.getUserId().getUserId().toString());

    // then
    assertThat(foundUser).isNotNull();
    assertThat(foundUser.getName()).isEqualTo(user.getUserFullName().getName());
  }

  @Test
  @DisplayName("UUID로 사용자 조회시 존재하지 않는 UUID일때 예외발생")
  void givenDuplicateUUIDWhenCallGetThenThrowException() {
    // given
    UUID uuid = UUID.randomUUID();

    // when
    // then
    String message = assertThrows(UserNotFoundException.class,
        () -> applicationService.get(uuid.toString())).getMessage();
    assertThat(message).contains("존재하지 않는 사용자입니다.");
  }

  @Test
  @DisplayName("userId와 name으로 사용자 정보 수정 성공")
  void givenUserIdAndNameWhenCallUpdateThenVoid() {
    // given
    String originNameStr = "abc";
    UserFullName originUserFullName = new UserFullName(originNameStr);
    User originUser = new User(originUserFullName);

    repository.save(originUser);

    String newNameStr = "def";
    String userId = originUser.getUserId().getUserId().toString();

    // when
    UserUpdateCommand command = new UserUpdateCommand(userId, newNameStr);
    applicationService.update(command);

    // then
    User foundUser = repository.findById(originUser.getUserId()).get();
    assertThat(foundUser.getUserFullName().getName()).isEqualTo(newNameStr);
  }

  @Test
  @DisplayName("UserDeleteCommand로 사용자 삭제 성공")
  void givenUserDeleteCommandWhenCallDeleteThenVoid() {
    // given
    User user = new User(new UserFullName("abc"));
    repository.save(user);
    UserDeleteCommand command = new UserDeleteCommand(user.getUserId().getUserId().toString());

    // when
    applicationService.delete(command);

    // then
    UUID uuid = UUID.fromString(command.getId());
    UserId id = new UserId(uuid);
    User foundUser = repository.findById(id).orElse(null);
    assertThat(foundUser).isNull();
  }

}