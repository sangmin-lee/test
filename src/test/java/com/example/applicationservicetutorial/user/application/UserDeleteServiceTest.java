package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserId;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@Transactional
class UserDeleteServiceTest {

  @Autowired
  UserDeleteService service;

  @Autowired
  UserRepository repository;

  @Test
  @DisplayName("회원탈퇴 성공")
  void givenUserDeleteCommandWhenCallHandleThenVoid() {
    // given
    User user = new User(new UserFullName("abc"));
    repository.save(user);
    UserDeleteCommand userDeleteCommand =
        new UserDeleteCommand(user.getUserId().getUserId().toString());

    // when
    service.handle(userDeleteCommand);

    // then
    UUID uuid = UUID.fromString(userDeleteCommand.getId());
    UserId userId = new UserId(uuid);
    boolean present = repository.findById(userId).isPresent();
    assertThat(present).isFalse();
  }

}