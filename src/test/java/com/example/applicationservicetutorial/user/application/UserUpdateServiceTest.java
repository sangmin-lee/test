package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@Transactional
class UserUpdateServiceTest {

  @Autowired
  UserUpdateService service;
  @Autowired
  UserRepository repository;

  @Test
  @DisplayName("내 정보 수정 성공")
  void givenUserUpdateCommandWhenCallHandleThenVoid() {
    // given
    String originName = "abc";
    User user = new User(new UserFullName(originName));
    repository.save(user);

    String newName = "qwe";
    String uuid = user.getUserId().getUserId().toString();
    UserUpdateCommand command = new UserUpdateCommand(uuid, newName);

    // when
    service.handle(command);

    // then
    boolean present = repository.existsByUserFullName(new UserFullName(newName));
    assertThat(present).isTrue();
  }

}