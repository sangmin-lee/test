package com.example.applicationservicetutorial.user.application;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class UserServiceTest {

  @Autowired
  UserRepository repository;

  @Test
  @DisplayName("사용자명 중복 확인")
  void givenNameWhenCallFindByNameThenReturnTrue() {
    // given
    UserFullName userFullName = new UserFullName("abc");
    User user = new User(userFullName);
    repository.save(user);

    // when
    boolean present = repository.existsByUserFullName(userFullName);

    // then
    assertThat(present).isTrue();
  }

}