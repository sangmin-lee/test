package com.example.applicationservicetutorial.user.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class NameTest {

  @Test
  @DisplayName("이름 생성 성공")
  void givenNameWhenCreateNameThenReturnUserName() {
    // given
    String name = "abc";

    // when
    UserFullName createdUserFullName = new UserFullName(name);

    // then
    assertThat(createdUserFullName.getName()).isEqualTo(name);
  }


  @Test
  @DisplayName("3글자 미만인 이름을 생성하여 예외 발생")
  void givenNameLengthLessThan3WhenCreateNameThenThrowException() {
    // given
    String name = "ab";

    // when
    // then
    assertThatThrownBy(() -> new UserFullName(name))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  @DisplayName("20글자 초과인 이름을 생성하여 예외 발생")
  void givenNameLengthLongerThan20WhenCreateNameThenThrowException() {
    // given
    String name = "123456789012345678901";

    // when
    // then
    assertThatThrownBy(() -> new UserFullName(name))
        .isInstanceOf(IllegalArgumentException.class);
  }

}