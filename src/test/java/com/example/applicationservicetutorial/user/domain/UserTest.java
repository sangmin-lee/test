package com.example.applicationservicetutorial.user.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@DataJpaTest
@Transactional
public class UserTest {

  @Test
  @DisplayName("Name으로 User 생성 성공")
  void givenNameWhenCreateUserThenReturnUser() {
    // given 
    UserFullName userFullName = new UserFullName("abc");

    // when
    User user = new User(userFullName);

    // then
    assertThat(user.getUserFullName()).isEqualTo(userFullName);
    assertThat(user.getUserId()).isNotNull();
  }

  @Test
  @DisplayName("User 생성시 Name이 Null일때 예외 발생")
  void givenNullWhenCreateUserThenThrowException() {
    // given
    UserFullName userFullName = null;

    // when
    // then
    assertThatThrownBy(() -> new User(userFullName))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  @DisplayName("UserId와 Name으로 User 생성 성공")
  void givenUserIdAndNameWhenCreateUserThenReturnUser() {
    // given
    UserId userId = new UserId(UUID.randomUUID());
    UserFullName userFullName = new UserFullName("abc");

    // when
    User user = new User(userId, userFullName);

    // then
    assertThat(user.getUserFullName()).isEqualTo(userFullName);
    assertThat(user.getUserId()).isEqualTo(userId);
  }

  @Test
  @DisplayName("User 생성시 UserId가 Null이거나 Name이 Null일때 예외 발생")
  void givenNullNameOrNullUserIdWhenCreateUserThenThrowException() {
    // given
    UserId userId = null;
    UserFullName userFullName = null;

    // when
    // then
    assertThatThrownBy(() -> new User(userId, userFullName))
        .isInstanceOf(IllegalArgumentException.class);
    assertThatThrownBy(() -> new User(new UserId(UUID.randomUUID()), userFullName))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  @DisplayName("User의 Name 변경 성공")
  void givenUserAndAnotherNameWhenChangeNameThenChangedName() {
    // given
    String origin = "abc";
    String another = "def";
    UserFullName originUserFullName = new UserFullName(origin);
    UserFullName anotherUserFullName = new UserFullName(another);
    User user = new User(originUserFullName);

    // when
    user.changeName(anotherUserFullName);

    // then
    assertThat(user.getUserFullName()).isEqualTo(anotherUserFullName);
  }

  @Test
  @DisplayName("User의 Name 변경시 Name이 Null일때 예외 발생")
  void givenUserAndNullNameWhenChangeNameThenThrowException() {
    // given
    UserFullName originUserFullName = new UserFullName("abc");
    UserFullName anotherUserFullName = null;
    User user = new User(originUserFullName);

    // when
    // then
    assertThatThrownBy(() -> user.changeName(anotherUserFullName))
        .isInstanceOf(IllegalArgumentException.class);
  }
  
}
