package com.example.applicationservicetutorial.user.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class UserIdTest {

  @Test
  @DisplayName("UserId 생성 성공")
  void givenUUIDWhenCreateUserIdThenReturnUserId() {
    // given
    UUID uuid = UUID.randomUUID();

    // when
    UserId userId = new UserId(uuid);

    // then
    assertThat(userId.getUserId()).isEqualTo(uuid);
  }

  @Test
  @DisplayName("Null인 id를 UserId로 생성하여 예외 발생")
  void givenNullWhenCreateUserIdThenThrowException() {
    // given
    UUID uuid = null;

    // when
    // then
    assertThatThrownBy(() -> new UserId(uuid))
        .isInstanceOf(IllegalArgumentException.class);
  }

}