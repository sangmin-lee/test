package com.example.applicationservicetutorial.user.repository;

import com.example.applicationservicetutorial.user.domain.UserFullName;
import com.example.applicationservicetutorial.user.domain.User;
import com.example.applicationservicetutorial.user.domain.UserId;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@Transactional
class UserRepositoryTest {

  @Autowired
  UserRepository repository;

  @Test
  @DisplayName("UserId로 User 찾기")
  void givenUserIdWhenFindByIdThenReturnUser() {
    // given
    UUID uuid = UUID.randomUUID();
    UserId userId = new UserId(uuid);
    String nameStr = "abc";
    UserFullName userFullName = new UserFullName(nameStr);

    User user = new User(userId, userFullName);
    repository.save(user);

    // when
    User foundUser = repository.findById(userId).get();

    // then
    assertThat(foundUser).isNotNull();
    assertThat(foundUser.getUserFullName()).isEqualTo(userFullName);
    assertThat(foundUser.getUserId()).isEqualTo(userId);
  }

  @Test
  @DisplayName("Name으로 User 찾기")
  void givenNameWhenCallFindByNameThenReturnUser() {
    // given
    UserFullName userFullName = new UserFullName("abc");
    User user = new User(userFullName);
    repository.save(user);

    // when
    boolean exists = repository.existsByUserFullName(userFullName);

    // then
    assertThat(exists).isTrue();
  }

}